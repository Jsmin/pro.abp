﻿using Microsoft.AspNetCore.Mvc;

namespace Volo.Abp.AspNetCore.Mvc.UI.Theme.Lepton.Themes.Lepton.Components.Header
{
    public class HeaderViewComponent : LeptonViewComponentBase
	{
		public IViewComponentResult Invoke()
		{
			return base.View("~/Themes/Lepton/Components/Header/Default.cshtml");
		}
	}
}
