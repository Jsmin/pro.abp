﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.AspNetCore.Mvc.UI.Theme.Lepton.Themes.Lepton.Components.Toolbar.LanguageSwitch;
using Volo.Abp.AspNetCore.Mvc.UI.Theme.Lepton.Themes.Lepton.Components.Toolbar.UserMenu;
using Volo.Abp.AspNetCore.Mvc.UI.Theme.Shared.Toolbars;
using Volo.Abp.Localization;
using Volo.Abp.Users;

namespace Volo.Abp.AspNetCore.Mvc.UI.Theme.Lepton.Toolbars
{
	public class LeptonThemeMainTopToolbarContributor : IToolbarContributor
	{
		public async Task ConfigureToolbarAsync(IToolbarConfigurationContext context)
		{
			if (!(context.Toolbar.Name != "Main"))
			{
				if (context.Theme is LeptonTheme)
				{
					var service = context.ServiceProvider.GetService<ILanguageProvider>();
					var readOnlyList = await service.GetLanguagesAsync();
					if (readOnlyList.Count > 1)
					{
						context.Toolbar.Items.Add(new ToolbarItem(typeof(LanguageSwitchViewComponent), 0));
					}
					if (context.ServiceProvider.GetRequiredService<ICurrentUser>().IsAuthenticated)
					{
						context.Toolbar.Items.Add(new ToolbarItem(typeof(UserMenuViewComponent), 0));
					}
				}
			}
		}
	}
}
