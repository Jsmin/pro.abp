﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;
using Volo.Abp.IdentityServer.ClaimTypes.Dtos;

namespace Volo.Abp.IdentityServer.ClaimTypes
{
	public interface IIdentityServerClaimTypeAppService : IRemoteService, IApplicationService
	{
		Task<List<IdentityClaimTypeDto>> GetListAsync();
	}
}
