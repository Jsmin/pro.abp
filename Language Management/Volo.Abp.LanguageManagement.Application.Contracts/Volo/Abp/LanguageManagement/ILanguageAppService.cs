﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.LanguageManagement.Dto;

namespace Volo.Abp.LanguageManagement
{
	public interface ILanguageAppService : IRemoteService, IApplicationService
	{
		Task<ListResultDto<LanguageDto>> GetAllListAsync();

		Task<ListResultDto<LanguageDto>> GetListAsync(GetLanguagesTextsInput input);

		Task<LanguageDto> UpdateAsync(Guid id, UpdateLanguageDto input);

		Task<LanguageDto> CreateAsync(CreateLanguageDto input);

		Task<LanguageDto> GetAsync(Guid id);

		Task DeleteAsync(Guid id);

		Task SetAsDefaultAsync(Guid id);

		Task<List<LanguageResourceDto>> GetResourcesAsync();

		Task<List<CultureInfoDto>> GetCulturelistAsync();
	}
}
