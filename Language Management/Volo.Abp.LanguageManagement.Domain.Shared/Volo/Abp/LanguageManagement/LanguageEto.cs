﻿using System;

namespace Volo.Abp.LanguageManagement
{
    [Serializable]
	public class LanguageEto
	{
		public Guid Id { get; set; }

		public string CultureName { get; set; }

		public string UiCultureName { get; set; }

		public string DisplayName { get; set; }

		public string FlagIcon { get; set; }

		public bool IsEnabled { get; set; }
	}
}
