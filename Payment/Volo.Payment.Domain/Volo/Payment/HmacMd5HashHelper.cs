﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Volo.Payment
{
    public static class HmacMd5HashHelper
	{
		public static string GetMd5Hash(string hashString, string signature)
		{
			return BitConverter.ToString(new HMACMD5(Encoding.UTF8.GetBytes(signature)).ComputeHash(Encoding.UTF8.GetBytes(hashString))).Replace("-", string.Empty).ToLowerInvariant();
		}
	}
}
