﻿using Volo.Abp.Localization;
using Volo.Abp.Localization.ExceptionHandling;
using Volo.Abp.Modularity;
using Volo.Abp.VirtualFileSystem;
using Volo.Payment.Localization;

namespace Volo.Payment
{
    [DependsOn(
		typeof(AbpPaymentDomainSharedModule)
	)]
	public class AbpPaymentDomainModule : AbpModule
	{
		public override void ConfigureServices(ServiceConfigurationContext context)
		{
			base.Configure<AbpVirtualFileSystemOptions>(options => options.FileSets.AddEmbedded<AbpPaymentDomainModule>());
			base.Configure<AbpLocalizationOptions>(options => options.Resources.Get<PaymentResource>().AddVirtualJson("/Volo/Payment/Localization/Domain"));
			base.Configure<AbpExceptionLocalizationOptions>(options => options.MapCodeNamespace("Payment", typeof(PaymentResource)));
		}
	}
}
