﻿using Microsoft.EntityFrameworkCore;
using System;
using Volo.Abp.EntityFrameworkCore.Modeling;
using Volo.Abp.TextTemplateManagement.TextTemplates;

namespace Volo.Abp.TextTemplateManagement.EntityFrameworkCore
{
    public static class TextTemplateManagementDbContextModelCreatingExtensions
	{
		public static void ConfigureTextTemplateManagement(this ModelBuilder builder, Action<TextTemplateManagementModelBuilderConfigurationOptions> optionsAction = null)
		{
			Check.NotNull<ModelBuilder>(builder, nameof(builder));
			var options = new TextTemplateManagementModelBuilderConfigurationOptions(
				TextTemplateManagementDbProperties.DbTablePrefix,
				TextTemplateManagementDbProperties.DbSchema
			);
			optionsAction?.Invoke(options);
			builder.Entity<TextTemplateContent>(b =>
			{
				b.ToTable<TextTemplateContent>(options.TablePrefix + "TextTemplateContents", options.Schema);
				b.ConfigureByConvention();
				b.Property<string>(x => x.Name).IsRequired().HasMaxLength(TextTemplateConsts.MaxNameLength);
				b.Property<string>(x => x.CultureName).HasMaxLength(TextTemplateConsts.MaxCultureNameLength);
				b.Property<string>(x => x.Content).IsRequired(true).HasMaxLength(TextTemplateConsts.MaxContentLength);
			});
		}
	}
}
