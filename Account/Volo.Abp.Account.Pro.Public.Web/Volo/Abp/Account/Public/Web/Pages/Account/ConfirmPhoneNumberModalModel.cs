﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Volo.Abp.Settings;

namespace Volo.Abp.Account.Public.Web.Pages.Account
{
    public class ConfirmPhoneNumberModalModel : AccountPageModel
	{
		[BindProperty]
		public string PhoneConfirmationToken { get; set; }

		public string PhoneNumber { get; set; }

		public ConfirmPhoneNumberModalModel(IAccountAppService accountAppService)
		{
			this.AccountAppService = accountAppService;
		}

		public virtual async Task OnGetAsync()
		{
			var flag = await base.SettingProvider.GetAsync<bool>("Abp.Identity.SignIn.EnablePhoneNumberConfirmation", true);
			if (!flag)
			{
				throw new BusinessException("Volo.Account:PhoneNumberConfirmationDisabled");
			}
			this.PhoneNumber = base.CurrentUser.PhoneNumber;
			await this.AccountAppService.SendPhoneNumberConfirmationTokenAsync();
		}

		public virtual async Task OnPostAsync()
		{
			await this.AccountAppService.ConfirmPhoneNumberAsync(new ConfirmPhoneNumberInput
			{
				Token = this.PhoneConfirmationToken
			});
		}
	}
}
