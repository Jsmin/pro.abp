﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Volo.Abp.Account.Public.Web.Pages.Account
{
	public class LogoutModel : AccountPageModel
	{
		[HiddenInput]
		[BindProperty(SupportsGet = true)]
		public string ReturnUrl { get; set; }

		[HiddenInput]
		[BindProperty(SupportsGet = true)]
		public string ReturnUrlHash { get; set; }

		public virtual async Task<IActionResult> OnGetAsync()
		{
			await base.SignInManager.SignOutAsync();
			IActionResult result;
			if (this.ReturnUrl != null)
			{
				result = base.RedirectSafely(this.ReturnUrl, this.ReturnUrlHash);
			}
			else
			{
				result = this.RedirectToPage("~/Account/Login");
			}
			return result;
		}

		public virtual Task<IActionResult> OnPostAsync()
		{
			return Task.FromResult<IActionResult>(this.Page());
		}
	}
}
