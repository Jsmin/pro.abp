﻿using System.Collections.Generic;

namespace Volo.Abp.AuditLogging
{
    public class GetErrorRateOutput
	{
		public Dictionary<string, long> Data { get; set; }
	}
}
