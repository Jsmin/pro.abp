﻿using AutoMapper;

namespace Volo.Abp.AuditLogging
{
    public class AbpAuditLoggingApplicationAutoMapperProfile : Profile
	{
		public AbpAuditLoggingApplicationAutoMapperProfile()
		{
			base.CreateMap<AuditLog, AuditLogDto>().MapExtraProperties<AuditLog, AuditLogDto>();
			base.CreateMap<EntityChange, EntityChangeDto>().MapExtraProperties<EntityChange, EntityChangeDto>();
			base.CreateMap<EntityChangeWithUsername, EntityChangeWithUsernameDto>();
			base.CreateMap<EntityPropertyChange, EntityPropertyChangeDto>();
			base.CreateMap<AuditLogAction, AuditLogActionDto>().MapExtraProperties<AuditLogAction, AuditLogActionDto>();
		}
	}
}
